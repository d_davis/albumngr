<?php
/*
Plugin Name: Album Manager
Description: Wordpress plugin for albums managment
Version:     201704
Author:      ddavis
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

//TinyMCE name adjustment for post request.
function enqueue_scripts(){
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/bootstrap.min.js');
	wp_enqueue_script('formCreator', get_template_directory_uri() . '/formCreator.js');
}

add_action('admin_enqueue_scripts', 'enqueue_scripts');


function editor(){
	wp_editor('<pre>Annotation

		Use pre tag first to save the syntax
		of your annotation.
		Just put it into text mode of this editor 
		and keep all contents inside it.</pre>', 'Annotation', array(
	    'textarea_name' => 'Annotation', 
	    'textarea_rows' => 10));
}

function transliterate_from_Russian($post_form_input_string_in_Russian) {
	$transliteration = str_replace(" ", "-", transliterator_transliterate('Russian-Latin/BGN', $post_form_input_string_in_Russian));
	return $transliteration;
}

/*function sanitize_title($title) { //function from cyr2lat plugin

	$iso9_table = array(
		'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Ѓ' => 'G`',
		'Ґ' => 'G`', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Є' => 'YE',
		'Ж' => 'ZH', 'З' => 'Z', 'Ѕ' => 'Z', 'И' => 'I', 'Й' => 'Y',
		'Ј' => 'J', 'І' => 'I', 'Ї' => 'YI', 'К' => 'K', 'Ќ' => 'K',
		'Л' => 'L', 'Љ' => 'L', 'М' => 'M', 'Н' => 'N', 'Њ' => 'N',
		'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
		'У' => 'U', 'Ў' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'TS',
		'Ч' => 'CH', 'Џ' => 'DH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '``',
		'Ы' => 'YI', 'Ь' => '`', 'Э' => 'E`', 'Ю' => 'YU', 'Я' => 'YA',
		'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'ѓ' => 'g',
		'ґ' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'є' => 'ye',
		'ж' => 'zh', 'з' => 'z', 'ѕ' => 'z', 'и' => 'i', 'й' => 'y',
		'ј' => 'j', 'і' => 'i', 'ї' => 'yi', 'к' => 'k', 'ќ' => 'k',
		'л' => 'l', 'љ' => 'l', 'м' => 'm', 'н' => 'n', 'њ' => 'n',
		'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
		'у' => 'u', 'ў' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts',
		'ч' => 'ch', 'џ' => 'dh', 'ш' => 'sh', 'щ' => 'shh', 'ь' => '',
		'ы' => 'yi', 'ъ' => "'", 'э' => 'e`', 'ю' => 'yu', 'я' => 'ya'
	);	

	$title = preg_replace("/[^A-Za-z0-9`'_\-\.]/", '-', $iso9_table);

	return $title;
}*/

//Function edit is for edit or delete data from table when post request was sent from table in admin view. $table is equals to table name, $data is name for column which is determined to find row to delete.
/*function edit($table, $data) {

	$edit = $_POST["edit"];
	$delete = $_POST["delete"];

	echo $table, $data, $delete;


	//$result = $wpdb->get_results("SELECT * FROM $table WHERE $data=$edit");
	//echo $result;

	$wpdb->delete($table, array($data => $delete));	

}*/
//This function is used to upload images. Value $url is writing down to the database.
function upload_image($dir) {

	$target_dir = $dir;
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
	    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
	    if($check !== false) {
		       echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
	    } else {
		       echo "File is not an image.";
		       $uploadOk = 0;
	    }
	}
	// Check if file already exists
	/*if (file_exists($target_file)) {
		   echo "Sorry, file already exists.";
		   $uploadOk = 0;
	}*/
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 50000000) {
		   echo "Sorry, your file is too large.";
		   $uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
			echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		   echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
		$id = uniqid();
		$newfilename = $id . '.' . $imageFileType;
		$newdirpath = $target_dir . $newfilename;

		/*$newfilename_phone = $id . bin2hex(random_bytes(12)) . '.' . $imageFileType;
		$newfilename_tablet = $id . bin2hex(random_bytes(12)) . '.' . $imageFileType;
		$newfilename_portable = $id . bin2hex(random_bytes(12)) . '.' . $imageFileType;
		$newfilename_pc = $id . bin2hex(random_bytes(12)) . '.' . $imageFileType;

		$newdirpath_phone = $target_dir . $newfilename_phone;
		$newdirpath_tablet = $target_dir . $newfilename_tablet;
		$newdirpath_portable = $target_dir . $newfilename_portable;
		$newdirpath_pc = $target_dir . $newfilename_pc;*/

		$newfilename_resized = $id . bin2hex(random_bytes(12)) . '.' . $imageFileType;
		$newdirpath_resized = $target_dir . $newfilename_resized;

		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $newdirpath)) {
		    echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		    $image = wp_get_image_editor($newdirpath); // Return an implementation that extends WP_Image_Editor
 
			if (!is_wp_error($image)) {
			    $image->resize(719, 719, false);
			    $image->save($newdirpath_resized);
			    /*$image->resize(500, 500, false);
			    $image->save($newdirpath_tablet);
			    $image->resize(400, 400, false);
			    $image->save($newdirpath_portable);
			    $image->resize(300, 300, false);
			    $image->save($newdirpath_pc);*/
			}
					    
		return array($newfilename_resized); //will return array of image fienames to add them into the db. 
		} else {
		    echo "Sorry, there was an error uploading your file.";
		}
	}
}


add_action('admin_menu', 'artists_menu');
add_action('admin_menu', 'albums_menu');
add_action('admin_menu', 'tracks_menu');


function artists_menu() {
	add_menu_page( 'Artists', 'Artists', 'manage_options', 'artists-manager', 'artists_options'  );
}

function albums_menu() {
	add_menu_page( 'Albums', 'Albums', 'manage_options', 'albums-manager', 'albums_options'  );
}

function tracks_menu() {
	add_menu_page( 'Tracks', 'Tracks', 'manage_options', 'tracks-manager', 'tracks_options'  );
}


function artists_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	} 

	echo '<div class="wrap">';
	echo '<form method="post" enctype="multipart/form-data">';
	echo '<table>';
	echo '<tr><td>Artist:</td><td><input type="text" name="Artist"></td></tr>';
	echo '<tr><td>Image:</td> <td><input type="file" name="fileToUpload" id="fileToUpload"></td></tr>';
	echo '</table>';
	editor();
	echo '<input type="submit" value="Send">';
	echo '</form>';
	echo '</div>';

	$artist = $_POST["Artist"];
	$annotation = $_POST["Annotation"];

	$dir = str_replace("\\", "/", WP_CONTENT_DIR) . "/" . "artists/";
	$images = upload_image($dir);

	$path_to_image = content_url() . "/" . "artists/";

	$image = $path_to_image . $images[0];

	if ($artist !='' And $images !='' And $annotation !='') {

		$artist_tr = transliterate_from_Russian($artist);

		global $wpdb;

		$wpdb->insert( 'mngr_artists', array(
			'Name' => $artist,
			'Name_tr' => $artist_tr,
			'image' => $image,
			'Annotation' => $annotation )
			);
	} else {
		echo 'One or more fields is empty';
	}

	global $wpdb;

	$result = $wpdb->get_results("SELECT Name, image, Annotation FROM mngr_artists");
	echo "<form method='post'>";
	if(!empty($result)) {
		echo "<table id='albums'>";
		echo "<tr>";
		echo "<th>Artist</th>";
		echo "<th>Image</th>";
		echo "<th>Annotation</th>";
		echo "<th></th>";
		echo "<th></th>";
		echo "</tr>";

		
		foreach($result as $r) {
			echo "<tr>";
	    	echo "<td>".$r->Name."</td>";
	    	echo "<td><img src='".$r->image."'width='128' height='128'></td>";
	    	echo "<td>".$r->Annotation."</td>";
	    	
	    	echo "<td><button type='submit' name='edit' value='".$r->Name."'>Edit</td>";
	    	echo "<td><button type='submit' name='delete' value='".$r->Name."'>Delete</td>";
	    	echo "</tr>";
	    	
			}
		} else {
	  		echo "<p>The database is empty!</p>";
	} echo "</table>";
	echo "</form>";

	$edit = $_POST["edit"];
	$delete = $_POST["delete"];
	$wpdb->delete('mngr_artists', array('Name' => $delete));
	$wpdb->delete('mngr_albums', array('Artist' => $delete));
	$wpdb->delete('mngr_tracks', array('Artist' => $delete));
	
}

function albums_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	} 

	echo '<div class="wrap">';
	echo '<form method="post" enctype="multipart/form-data">';
	echo '<table>';
	echo '<tr><td>Artist:</td><td><select name="Artist">';
	global $wpdb;
	$result = $wpdb->get_results("SELECT Name FROM mngr_artists");
	
	foreach($result as $r) {
		echo '<option value="'.$r->Name.'">'.$r->Name.'</option>';
	}
	echo '</select></td></tr>';

	echo '<tr><td>Album:</td><td> <input type="text" name="Album" required></td></tr>';
	echo '<tr><td>Image:</td><td> <input type="file" name="fileToUpload" id="fileToUpload" accept="image/*" required></td></tr>';
	echo '<tr><td>Genre:</td><td> <input type="text" name="Genre" required></td></tr>';
	echo '<tr><td>Release date:</td><td> <input type="date" value="'.date('Y-m-d').'" name="Releasedate" required></td></tr>';
	echo '<tr><td>iTunes:</td><td> <input type="url" name="itunes">Leave it free if none</td></tr>';
	echo '<tr><td>Google Play:</td><td> <input type="url" name="googleplay">Leave it free if none</td></tr>';
	echo '</table>';
	editor();
	echo '<input type="submit" value="Send">';
	echo '</form>';
	echo '</div>';

	$artist = $_POST["Artist"];
	$album = $_POST["Album"];	
	$genre = $_POST["Genre"];
	$releasedate = $_POST["Releasedate"];
	$annotation = $_POST["Annotation"];
	$image = $_POST["fileToUpload"];
	$itunes = $_POST["itunes"];
	$googleplay = $_POST["googleplay"];

	$uniq_dir = uniqid();
	$dir = str_replace("\\", "/", WP_CONTENT_DIR) . "/" . "albums/" . $uniq_dir . "/";

	$check_album_exist = $wpdb->get_results("SELECT Artist, Album FROM mngr_albums WHERE Artist='". $artist ."' AND Album='". $album ."'");

	$in_table = $wpdb->num_rows;

	if($in_table == 0 and $artist !='' and $album !='' and $genre != '' and $releasedate != '' and $annotation !='') {
		mkdir($dir, 0775);
		echo 'created directory' . $dir;
		$uploadOk = 1;
	} elseif ($in_table > 0){
		echo '<p>This album is already exists</p>';
		$uploadOk = 0;
	}

	echo " " . $artist . " " . $album . " " . $genre . " " . $releasedate . " " . $annotation . " " . $image . " " . $artist_tr;
	
	$album_url = content_url() . "/" . "albums/" . $uniq_dir . "/";

	$images = upload_image($dir);

	$path_to_image = content_url() . "/" .  "albums/" . $uniq_dir . "/";

	$image = $album_url . $images[0];

	if ($artist !='' and $album !='' and $genre != '' and $releasedate != '' and $annotation !='' and $uploadOk = 1 and $images !='') {

		$artist_tr = transliterate_from_Russian($artist);
		$album_tr = transliterate_from_Russian($album);
		$genre_tr = transliterate_from_Russian($genre);

		global $wpdb;
		$wpdb->insert('mngr_albums', array(
			'Artist' => $artist,
			'Album' => $album,
			'Genre' => $genre,
			'Artist_tr' => $artist_tr,
			'Album_tr' => $album_tr,
			'Genre_tr' => $genre_tr,
			'Releasedate' => $releasedate,
			'image' => $image,
			'Annotation' => $annotation,
			'AlbumURL' => $album_url, 
			'AlbumDir' => $dir,
			'itunes' => $itunes,
			'googleplay' => $googleplay));
	} else {
		echo 'One or more fields is empty';
	}

	global $wpdb;

	$result = $wpdb->get_results("SELECT Artist, Album, image, Genre, Releasedate, Annotation, itunes, googleplay FROM mngr_albums ORDER BY Releasedate");
	echo '<form method="post">';
	if(!empty($result)) {
		echo "<table id='albums'>";
		echo "<tr>";
		echo "<th>Artist</th>";
		echo "<th>Album</th>";
		echo "<th>Image</th>";
		echo "<th>Genre</th>";
		echo "<th>Release date</th>";
		echo "<th>Annotation</th>";
		echo "<th>iTunes</th>";
		echo "<th>Google Play</th>";
		echo "<th></th>";
		echo "<th></th>";
		echo "</tr>";

		foreach($result as $r) {
			echo "<tr>";
	    	echo "<td>".$r->Artist."</td>";
	    	echo "<td>".$r->Album."</td>";
	    	echo "<td><img src='".$r->image."'width='128' height='128'></td>";
	    	echo "<td>".$r->Genre."</td>";
	    	echo "<td>".$r->Releasedate."</td>";
	    	echo "<td>".$r->Annotation."</td>";
	    	echo "<td>".$r->itunes."</td>";
	    	echo "<td>".$r->googleplay."</td>";
	    	echo "<td><button type='submit' name='edit' value='".$r->Album."'>Edit</td>";
	    	echo "<td><button type='submit' name='delete' value='".$r->Album."'>Delete</td>";
	    	echo "</tr>";
			}
		} else {
		  	echo "<p>The database is empty!</p>";
	} echo "</table>"; 
	echo '</form>';

	$edit = $_POST["edit"];
	$delete = $_POST["delete"];
	$wpdb->delete('mngr_albums', array('Album' => $delete));
	$wpdb->delete('mngr_tracks', array('Album' => $delete));
}


function tracks_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	global $wpdb;
	$result = $wpdb->get_results("SELECT * FROM mngr_albums ORDER BY Releasedate");

	echo '<div class="wrap">';
	echo '<form method="post" enctype="multipart/form-data">';
	echo 'Album:  <select name="Album">';
	foreach($result as $r) {
	echo '<option value="'.$r->Artist_tr.'_'.$r->Album_tr.'">'.$r->Artist.' - '.$r->Album.'</option>';
	}
	echo '</select><br>';
	echo 'Track: <input type="text" name="Track"><br>';
	echo 'File:  <input type="file" name="fileToUpload" id="fileToUpload"><br>';
	echo '<input type="submit">';
	echo '</form>';
	echo '</div>';

	$artist_album_tr = explode("_", $_POST["Album"]);
	$artist_tr = $artist_album_tr[0];
	$album_tr = $artist_album_tr[1];
	$track = $_POST["Track"];
	//$filename = $_POST["fileToUpload"];

	$ald = $wpdb->get_results("SELECT AlbumDir FROM mngr_albums WHERE Artist_tr='". $artist_tr ."' AND Album_tr='". $album_tr ."'");

	foreach($ald as $ad) {
		$albumdir = $ad->AlbumDir;
	}
	echo "albumdir" . $albumdir;
	$l = str_replace("\\", "/", WP_CONTENT_DIR) . "/". "albums/";
	//echo $albumdir;
	echo "l" . $l;
	$explode = explode($l, $albumdir);
	echo "exploded" . $explode[1];

	$dir = $albumdir;

	if(is_dir($dir)) {
		$uploadOk = 1;
	} else if(!is_dir($dir)) {
		$uploadOk = 0;
		echo '<p>Album directory is not exist</p>';
	}

	$target_dir = $dir;
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$dir_url = content_url() . "/" . "albums/" . $explode[1];
	$FileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
	    $check = is_file($_FILES["fileToUpload"]["tmp_name"]);
	    if($check == true) {
	        echo "File is - " . $_FILES["fileToUpload"]["type"] . ".";
	        $uploadOk = 1;
	    } else {
	        echo "File is not mp3.";
	        $uploadOk = 0;
	    }
	}
	// Check if file already exists
	if (file_exists($target_file)) {
	    echo "Sorry, file already exists.";
	    $uploadOk = 0;
	}
	// Check file size
	/*if ($_FILES["fileToUpload"]["size"] > 500000) {
	    echo "Sorry, your file is too large.";
	    $uploadOk = 0;
	}*/
	// Allow certain file formats
	if($FileType != "mp3" && $FileType != "mpeg") {
	    echo "Sorry, only mp3 files are allowed.";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
		$newfilename = (uniqid() . '.' . $FileType);
		$newdirpath = $target_dir . $newfilename;
	    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $newdirpath)) {
	        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
	    } else {
	        echo "Sorry, there was an error uploading your file.";
	        echo 'Error code ';
			print_r($_FILES["fileToUpload"]["error"]);
	    }
	    
	}

	$file_url = $dir_url . $newfilename;

	if ($track != '' && $uploadOk == 1) {
		global $wpdb;
		$wpdb->insert( 'mngr_tracks', array(
			'Artist_tr' => $artist_tr,
			'Album_tr' => $album_tr,
			'Track' => $track,
			'FileDir' => $file_url,
			'FileName' => $newfilename ) );
	} else {
		echo 'One or more fields is empty';
	}

	global $wpdb;

	$result = $wpdb->get_results("SELECT * FROM mngr_tracks ORDER BY Album_tr");
	echo '<form method="post">';
	if(!empty($result)) {
		echo "<table id='albums'>";
		echo "<tr>";
		echo "<th>Artist</th>";
		echo "<th>Album</th>";
		echo "<th>Track</th>";
		echo "<th>File</th>";
		echo "<th>URL</th>";
		echo "<th></th>";
		echo "<th></th>";
		echo "</tr>";

		foreach($result as $r) {
				echo "<tr>";
	    	echo "<td>".$r->Artist_tr."</td>";
	    	echo "<td>".$r->Album_tr."</td>";
	    	echo "<td>".$r->Track."</td>";
	    	echo "<td>".$r->FileName."</td>";
	    	echo "<td>".$r->FileDir."</td>";
	    	echo "<td><button type='submit' name='edit' value='".$r->Track."'>Edit</td>";
	    	echo "<td><button type='submit' name='delete' value='".$r->Track."'>Delete</td>";
	    	echo "</tr>";
		}
	} else {
	     	echo "<p>The database is empty!</p>";
	} echo "</table>";
	echo '</form>';

	$edit = $_POST["edit"];
	$delete = $_POST["delete"];
	$wpdb->delete('mngr_tracks', array('Track' => $delete));
}

?>


