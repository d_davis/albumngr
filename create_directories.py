#!/usr/bin/python

import os, sys

albums_dir = os.getcwd() + "/albums"
artists_dir = os.getcwd() + "/artists"

if sys.platform.startswith('win32'):

	os.mkdir(albums_dir)
	os.mkdir(artists_dir)

	print("Created directories: ", albums_dir, albums_dir)

elif sys.platform.startswith('linux2'):

	import pwd, grp

	uid = pwd.getpwnam("www-data").pw_uid
	gid = grp.getgrnam("www-data").gr_gid

	os.mkdir(albums_dir, 0744)
	os.chown(albums_dir, uid, gid)

	os.mkdir(artists_dir, 0744)
	os.chown(artists_dir, uid, gid)

	print("Created directories: ", albums_dir, albums_dir)