CREATE TABLE mngr_artists (
	Name char(255) NOT NULL, 
	Name_tr char(255) NOT NULL, 
	Annotation text NOT NULL, 
	image text NOT NULL
	) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE mngr_albums (
	Artist char(255) NOT NULL, 
	Album char(255) NOT NULL, 
	Genre char(255) NOT NULL, 
	Artist_tr char(255) NOT NULL, 
	Album_tr char(255) NOT NULL, 
	Genre_tr char(255) NOT NULL, 
	Releasedate date NOT NULL, 
	Annotation text NOT NULL, 
	AlbumURL text NOT NULL, 
	AlbumDir text NOT NULL, 
	image text NOT NULL,
	itunes text NULL DEFAULT NULL,
	googleplay text NULL DEFAULT NULL 
	) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE mngr_tracks (
	Artist_tr char(255) NOT NULL, 
	Album_tr char(255) NOT NULL, 
	Track char(255) NOT NULL, 
	FileDir text NOT NULL, 
	FileName text NOT NULL
	) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
